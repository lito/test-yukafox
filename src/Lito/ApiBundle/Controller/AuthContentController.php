<?php

namespace Lito\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use FOS\RestBundle\Util\Codes;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;

use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthContentController extends FOSRestController
{
    public function getLogContentManager() {
        return $this->get('lito_api.auth_content_manager');
    }
    
    /**
     * Add user auth token 
     * 
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * 
     * @Annotations\QueryParam(name="login", nullable=false, description="defines user login name")
     * @Annotations\QueryParam(name="password", nullable=false, description="defines user password")
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function postUserAuthAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $content = array(
            'login' => $paramFetcher->get('login'),
            'password' => $paramFetcher->get('password')
        );
        return $this->getLogContentManager()->add($content);
        
    }

    /**
     * Delete user auth token
     * 
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * 
     * @Annotations\QueryParam(name="token", nullable=false, description="defines user token")
     *
     * @Annotations\View(templateVar="content")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function deleteUserAuthAction(Request $request, ParamFetcherInterface $paramFetcher)
    {        
        return $this->getLogContentManager()->delete($paramFetcher->get('token'));
    }
}
