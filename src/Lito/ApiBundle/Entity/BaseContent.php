<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseContent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lito\ApiBundle\Entity\BaseContentRepository")
 * 
 */
class BaseContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="documentId", type="integer")
     */
    private $documentId;

    /**
     * @var string
     *
     * @ORM\Column(name="identifiedWith", length=255)
     */
    private $identifiedWith;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text")
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="body", type="text")
     */
    private $body;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="lastModified", type="datetime")
     */
    private $lastModified;    
   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get documentId
     *
     * @return integer 
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set identifiedWith
     *
     * @param string $identifiedWith
     * @return BaseContent
     */
    public function setIdentifiedWith($identifiedWith)
    {
        $this->identifiedWith = $identifiedWith;
    
        return $this;
    }

    /**
     * Get identifiedWith
     *
     * @return string 
     */
    public function getIdentifiedWith()
    {
        return $this->identifiedWith;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return BaseContent
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }
    
    /**
     * Set title
     * 
     * @param string $title
     * @return BaseContent
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }
    
    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
    * Set body
    *
    * @param string $body
    * @return BaseContent
    */
    public function setBody($body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set documentId
     *
     * @param integer $documentId
     * @return BaseContent
     */
    public function setDocumentId($documentId)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     * @return BaseContent
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime 
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }
}
