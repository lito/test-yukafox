<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Gallery
 *
 * @ORM\Table()
 * @ORM\Entity
 * 
 */
class Gallery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *    
     * @ORM\Column(name="headline", type="text")
     */
    private $headline;
    
    /**
     * @var string
     *    
     * @ORM\Column(name="subheadline", type="text")
     */
    private $subheadline;
    
    /**
     * @var string
     *    
     * @ORM\Column(name="buttonText", type="text")
     */
    private $buttonText;
    
    /**
     * @var string
     *    
     * @ORM\Column(name="buttonLink", type="text")
     */
    private $buttonLink;
    
    /**
     * @var integer
     *    
     * @ORM\Column(name="image", type="integer")
     */
    private $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="identifiedWith", length=255)
     */
    private $identifiedWith;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headline
     *
     * @param string $headline
     * @return Gallery
     */
    public function setHeadline($headline) {
        $this->headline = $headline;

        return $this;
    }
    
    /**
     * Get headline
     *
     * @return string 
     */
    public function getHeadline() {
        return $this->headline;
    }

    /**
     * Get subheadline
     *
     * @return string 
     */
    public function getSubheadline() {
        return $this->subheadline;
    }
    
    /**
     * Set subheadline
     *
     * @param string $subheadline
     * @return Gallery
     */
    public function setSubheadline($subheadline) {
        $this->subheadline = $subheadline;

        return $this;
    }
    
     /**
     * Set button text
     *
     * @param string $buttonText
     * @return Gallery
     */
    public function setButtonText($buttonText) {
        $this->buttonText = $buttonText;

        return $this;
    }
    
    /**
     * Get button text
     *
     * @return string 
     */
    public function getButtonText() {
        return $this->buttonText;
    }
    
    /**
     * Set button link
     *
     * @param string $buttonLink
     * @return Gallery
     */
    public function setButtonLink($buttonLink) {
        $this->buttonLink = $buttonLink;

        return $this;
    }
    
    /**
     * Get button link
     *
     * @return string 
     */
    public function getButtonLink() {
        return $this->buttonLink;
    }
    
    /**
     * Set image
     *
     * @param integer $image
     * @return Gallery
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }
    
    /**
     * Get image
     *
     * @return integer 
     */
    public function getImage() {
        return $this->image;
    }
    
    /**
     * Set identifiedWith
     *
     * @param string $identifiedWith
     * @return Gallery
     */
    public function setIdentifiedWith($identifiedWith)
    {
        $this->identifiedWith = $identifiedWith;
    
        return $this;
    }

    /**
     * Get identifiedWith
     *
     * @return string 
     */
    public function getIdentifiedWith()
    {
        return $this->identifiedWith;
    }
}