<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GalleryItem
 *
 * @ORM\Table()
 * @ORM\Entity
 * 
 */
class GalleryItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @var integer
     *
     * @ORM\Column(name="documentId", type="integer")
     */
    private $documentId;

    /**
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="album", referencedColumnName="id")
     */
    protected $album;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="orderId", type="integer")
     */
    private $orderId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set documentId
     *
     * @param string $documentId
     * @return GalleryItem
     */
    public function setDocumentId($documentId)
    {
        $this->documentId = $documentId;

        return $this;
    }

    /**
     * Get documentId
     *
     * @return integer 
     */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /**
     * Set album
     *
     * @param \Lito\ApiBundle\Entity\Album $album
     * @return Album
     */
    public function setAlbum(\Lito\ApiBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \Lito\ApiBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }
    
    /**
     * Set order id
     *
     * @param integer $orderId
     * @return GalleryItem
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get order id
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}
