<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Document
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="relatedToUser", type="integer")
     */
    private $relatedToUser;

      /**
     * @var integer
     *
     * @ORM\Column(name="showInMenu", type="integer")
     */
    private $showInMenu;
    
    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="category")
     */
    protected $documents;

    public function __construct() {
        $this->documents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add documents
     *
     * @param \Lito\ApiBundle\Entity\Document $documents
     * @return Category
     */
    public function addDocument(\Lito\ApiBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \Lito\ApiBundle\Entity\Document $documents
     */
    public function removeDocument(\Lito\ApiBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set relatedToUser
     *
     * @param integer $relatedToUser
     * @return Category
     */
    public function setRelatedToUser($relatedToUser)
    {
        $this->relatedToUser = $relatedToUser;

        return $this;
    }

    /**
     * Get relatedToUser
     *
     * @return integer 
     */
    public function getRelatedToUser()
    {
        return $this->relatedToUser;
    }


    /**
     * Set showInMenu
     *
     * @param integer $showInMenu
     * @return Category
     */
    public function setShowInMenu($showInMenu)
    {
        $this->showInMenu = $showInMenu;

        return $this;
    }

    /**
     * Get ShowInMenu
     *
     * @return integer 
     */
    public function getShowInMenu()
    {
        return $this->showInMenu;
    }
}
