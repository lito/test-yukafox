<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\AuthContent;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthContentManager {
    
    private $entityManager;
    private $ormRepository;
    private $adminPassword;

    public function __construct(EntityRepository $baseContentRepository, EntityManager $entityManager, $adminPassword)
    {
        $this->ormRepository = $baseContentRepository;
        $this->entityManager = $entityManager;
        $this->adminPassword = $adminPassword;
    }
    
    public function get($token)
    {
        $auth = $this->ormRepository->findBy(array('token' => $token));
        return $auth[0];
    }

    public function set($content, $data)
    {
        try {            
            if(empty($data['login']) || empty($data['password'])) {                
                throw new UnauthorizedHttpException('/api/user','Bad request. Not enough input parameters');
            } else {
                if($data['login'] === 'admin' && $data['password'] === $this->adminPassword) {
                    $content->setRelatedToUser(0);
                    $content->setToken(md5($data['login'].$data['password']));
                    $content->setIsAdmin(true);
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong credentials');
                }
            }            
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
       
        return $content;
    }

    public function delete($token)
    {   
        try{
            if(empty($token)) {
                throw new UnauthorizedHttpException('/api/user','Bad request. Not enough input parameters');
            } else {
                $this->entityManager->remove($this->get($token));        
                $this->entityManager->flush();
                return array(
                    "success" => true
                );
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
        
    }
    
    public function add($element)
    {
        $content = new AuthContent();
        $this->save($this->set($content, $element));        
        
        return array('token' => $content->getToken(), 'isAdmin' => $content->getIsAdmin());        
    }   
    
    private function save(AuthContent $authContent) {
        $this->entityManager->persist($authContent);
        $this->entityManager->flush();
    }
}
