<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Lito\ApiBundle\Entity\Document;
use DateTime;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class DocumentContentManager
{   
    protected $uploadsDir;
    protected $request;
    
    private $entityManager;
    private $documentRepository;
    private $categoryRepository;
    private $authRepository;
    
    private $thumbWidth;

    public function __construct(Container $container, EntityRepository $documentContentRepository, EntityManager $entityManager, EntityRepository $categoryContentRepository, EntityRepository $authContentRepository, $thumbnailWidth)
    {
        $this->documentRepository = $documentContentRepository;
        $this->categoryRepository = $categoryContentRepository;
        $this->authRepository = $authContentRepository;
        $this->entityManager = $entityManager;
        $this->request = $container->get('request');
        $this->uploadsDir = (in_array($container->get('kernel')->getEnvironment(), array('test', 'prod'))) ?
            // TBD path depends on vps / shared hosting. 
            // Deprecated since the current hostings use original SF structure
            __DIR__ . '/../../../../web/uploads/' :
            __DIR__ . '/../../../../web/uploads/';
        $this->thumbWidth = $thumbnailWidth;
        
    }
    
    private function setDocument(Document $document, UploadedFile $file)
    {
        $name = str_replace(" ", "_", strtolower($file->getClientOriginalName()));
        //$catName = $category;
        $document->setName($name);
        //$document->setFullPath($this->uploadsDir . $name);
        $now = new DateTime;
        $document->setCreatedAt($now);
        
        if(is_object($this->category)){
            $document->setCategory($this->category);
        }
        return $document;
    }
    
    private function getUploadsDir ($categoryName) {
        return $this->uploadsDir . $categoryName;
    }
    
    private function getCategory ($categoryId = null) {
        $this->category = null;
        if($categoryId) {
            $this->category = $this->categoryRepository->find($categoryId);
        }
        return $this->category;
    }

    public function add()
    {
        $this->category = $this->getCategory($this->request->get("category"));
        //TBD extend it with multi upload support
        if($this->request->getMethod() == "POST"){
            $file = $this->request->files->get('file');
            if($file instanceof UploadedFile) {
                
                if(isset($this->category)) {
                    $categoryName = $this->category->getName();
                }
                        
                $newName = str_replace(" ", "_", strtolower($file->getClientOriginalName()));
                if($file->move($this->getUploadsDir($categoryName).'/', $newName)){
                    $document = $this->save($this->setDocument(new Document(), $file));
                    $fs = new Filesystem();
                    if(!is_dir($this->getUploadsDir($categoryName).'/thumbnails/')) {
                        $fs->mkdir($this->getUploadsDir($categoryName).'/thumbnails/');
                    }                    
                    $orglimg  = $this->getUploadsDir($categoryName).'/'.$newName;
                    $outimg   = $this->getUploadsDir($categoryName).'/thumbnails/'.$newName;
                    $height = $this->getRatioHeight($orglimg, $this->thumbWidth);
                    $imagine   = new \Imagine\Gd\Imagine();                    
                    $imagine->open($orglimg)->resize(new \Imagine\Image\Box($this->thumbWidth,$height))->save($outimg);
                    if($document){
                        return array("status" => "success", "document" => $document);
                    }
                }
            }
            
        }

        return array("status" => "upload_failed");
      
    } 
    
    private function getRatioHeight($path, $newwidth)
    {
        list($width, $height, $type, $attr) = getimagesize($path);
        $newheight = round($newwidth * ($height/$width), 0);
        return $newheight;
    }
   
    public function getDocument($id) {
        // TBD through exception if object is not found 
        return $this->documentRepository->find($id);
    }

    public function delete($id)
    {
        $categoryName = '';
        $fullPathToDocument = '';
        $document = $this->getDocument($id);
        if (isset($document)){
            $category = $document->getCategory();
            if (isset($category)){
                $categoryName = $category->getName();
            }
            $documentName = $document->getName();
            $fullPathToDocument=$this->getUploadsDir($categoryName).'/'.$documentName;
            if ($fullPathToDocument && file_exists($fullPathToDocument)) {
                unlink($fullPathToDocument);
                unlink($this->getUploadsDir($categoryName).'/thumbnails/'.$documentName);
            }
            $this->entityManager->remove($document);
            $this->entityManager->flush();
            return ["success" => true];
        }
        return ["success" => false];
    }


    private function save(Document $document) {
        
        if(is_object($this->category)) {
            $this->entityManager->persist($this->category);
        }
        
        $this->entityManager->persist($document);
        $this->entityManager->flush();
        return $document;
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            
            throw new AccessDeniedHttpException('Access denied. No permission');
            
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
}
