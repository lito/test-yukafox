<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Service\ContentManagerInterface;
use Lito\ApiBundle\Entity\BaseContent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class BaseContentManager implements ContentManagerInterface
{   
    private $entityManager;
    private $ormRepository;
    private $authRepository;

    public function __construct(EntityRepository $baseContentRepository, EntityManager $entityManager, EntityRepository $documentRepository, EntityRepository $authContentRepository)
    {
        $this->ormRepository = $baseContentRepository;
        $this->documentRepository = $documentRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
    }

    public function fetch($start = 0, $limit = 5)
    {
        
    }
    
    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }

    public function transformObject ($element)
    {        
        $result = array(
            "id" => $element->getId(),
            "title" => $element->getTitle(),
            "body" => $element->getBody(),
            "lastModified" => date_format($element->getLastModified(), "d.m.Y H:i:s"),
            "documentId" => $element->getDocumentId()
        ); 

        $document = $this->documentRepository->find($element->getDocumentId()); 
        if($document) {
            $category = $document;
            $result["documentCategoryName"] = $document->getCategory()->getName();
            $result["documentName"] = $document->getName();
        }
        return $result;
    }
    
    public function getByIdentifier($identifier)
    {
        $results = array();
        $content = $this->ormRepository->findBy(
                array("identifiedWith" => $identifier));       
        if(count($content)) {            
            foreach ($content as $element) {                
                $results[] = $this->transformObject($element);                
            }        
        }
        return $results;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }

    public function set($content, $data)
    {
        if(isset($data["identifiedWith"])) {
            $content->setIdentifiedWith($data["identifiedWith"]);            
        } elseif(!$content->getIdentifiedWith()) {
            $content->setIdentifiedWith("");
        }        
        if(isset($data['documentId'])) {
            $content->setDocumentId($data["documentId"]);
        } elseif(!$content->getDocumentId()) {
            $content->setDocumentId(0);
        }        
        if(isset($data['link'])) {
            $content->setLink($data["link"]);
        } elseif(!$content->getLink()) {
            $content->setLink("");
        }
        $content->setTitle($data["title"]);
        $content->setBody($data["body"]);
        
        $content->setLastModified(new \DateTime('now'));
        
        return $content;
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
    }
    
    public function add($element)
    {
        $content = new BaseContent();
        $this->save($this->set($content, $element));
        
        return $content->getId();
    }
    
    public function updateById($id, $data)
    {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return array(
            "success" => true,
            "element" => $this->transformObject($element)
        );
        
    }
    
    private function save(BaseContent $baseContent) {
        $this->entityManager->persist($baseContent);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
