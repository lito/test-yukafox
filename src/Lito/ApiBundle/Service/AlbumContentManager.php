<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\GalleryItem;
use Lito\ApiBundle\Entity\Album;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class AlbumContentManager implements ContentManagerInterface{
    
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $documentRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $authContentRepository, EntityRepository $documentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository= $authContentRepository;
        $this->documentRepository = $documentRepository;
    }

    public function getAll()
    {        
        $content = $this->ormRepository->findAll();
                
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }
    
    public function transformObject($element) {
        
        $result = array(
            "id" => $element->getId(),
            "title" => $element->getTitle(),
            "body" => $element->getBody(),
            "intro" => $element->getIntro(),
            "link" => $element->getLink()
        ); 
        $documents = $element->getItems();
        $images = array();
        foreach ($documents as $item) {
            $id = $item->getDocumentId();
            $document = $this->documentRepository->find($id);
            if($document) {
                $images[] = array(
                    'id' => $id,
                    'title' => $item->getTitle(),
                    'documentCategoryName' => $document->getCategory()->getName(),
                    'documentName' => $document->getName(),
                    'orderId' => $item->getOrderId()
                );
            }
        }
        $result['items'] = $images;              
        return $result;
        
    }
    
    public function getByIdentifier($identifier)
    {
        
        $content = $this->ormRepository->findBy(
                array("identifiedWith" => $identifier));

       
        /*$results = array();
         * if(count($content)) {
            
            foreach ($content as $element) {
                
                $results[] = $this->transformObject($element);
                
            }
        
        }
        return $results;*/
        return $content;
    }

    public function set($content, $data)
    {
        // TBD Add automati setter if a key exists
        $content->setTitle($data["title"]);
        $content->setIntro($data["intro"]);
        $content->setBody($data["body"]);
        $content->setIdentifiedWith('works');
        $content->setLink($data["link"]);
        foreach ($content->getItems() as $item) {
            $content->removeItem($item);
            $this->entityManager->remove($item);
        }
        
        foreach ($data["items"] as $itemData) {
            $newItem = new GalleryItem();
            $newItem->setDocumentId($itemData["documentId"]);
            $newItem->setTitle($itemData["title"]);
            $newItem->setAlbum($content);
            $newItem->setOrderId($itemData['orderId']);
            $content->addItems($newItem);
            $this->entityManager->persist($newItem);
        }

        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new Album();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(Album $galleryContent)
    {
        $this->entityManager->persist($galleryContent);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
