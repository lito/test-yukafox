<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lito\ApiBundle\Service;

/**
 *
 * @author MaMay
 */
interface ContentManagerInterface {
    //put your code here
    public function getAll();
    
    public function get($id);
    
    public function set($element, $data);
    
    public function updateById($id, $data);
    
    public function add($newElement);
    
    public function delete($id);
}
