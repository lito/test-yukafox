<?php

namespace Lito\ApiBundle\Service;

class EmailManager {
	
	protected $mailer;
	private $admin_email;
	
	public function __construct (\Swift_Mailer $mailer, $admin_email) {
		
		$this->mailer = $mailer;
		$this->admin_email = $admin_email;
	}
	
	public function sendEmail($message) {
		
		if(is_array($message)) {
			
			$subject = 'General';
			
			$mail = \Swift_Message::newInstance()
					->setSubject($subject)
					->setFrom($message['email'])
					->setTo($this->admin_email)
					->setBody(
									
						"Hello!\n ".$message['textarea']."\n\n Best Regards, \n".$message['name']."\n________ \n\n".$message['name']."\nEmail: ".$message['email']
									
					);
						
			$this->mailer->send($mail);	
			
		}		
		
	}
	
}

?>