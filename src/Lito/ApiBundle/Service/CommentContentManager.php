<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\Comment;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class CommentContentManager implements ContentManagerInterface {
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $articleRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $authContentRepository, EntityRepository $articleRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
        $this->articleRepository = $articleRepository;
    }

    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        usort($content, function($a, $b) {
            $ad = $a->getDate();
            $bd = $b->getDate();

            if ($ad == $bd) {
              return 0;
            }

            return $ad < $bd ? 1 : -1;
        });
        
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }
    
    public function getAmount($amount) {        
        $content = $this->getAll();
        return array_slice($content, 0, $amount, true);
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }
    
    public function getByArticle($articleId) {
        $content = $this->getAll();
        foreach ($content as $key => $value) {
            if($value["article"] != $articleId) {
                unset($content[$key]);
            }
        }
        return $content;
    }
    
    public function transformObject ($element)
    {
        $result = array(
            "id" => $element->getId(),
            "author" => $element->getAuthor(),
            "body" => $element->getBody(),
            "date" => date_format($element->getDate(), "d.m.Y H:i:s"),
            "article" => $element->getArticle()->getId()
        );
        
        return $result;
    }

    public function set($content, $data)
    {        
        // TBD Add automati setter if a key exists
        try {
            $article = $this->articleRepository->find($data['article']);
            if($article) {
                $content->setAuthor($data["author"]);
                $content->setBody($data["body"]);        
                $content->setDate(new \DateTime('now'));
                $content->setArticle($article);
            } else {
                throw new AccessDeniedHttpException('Access denied. No such article');
            }            
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new Comment();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(Comment $comment)
    {
        $this->entityManager->persist($comment);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            return $this->add($element);            
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            throw new AccessDeniedHttpException('Access denied');            
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
