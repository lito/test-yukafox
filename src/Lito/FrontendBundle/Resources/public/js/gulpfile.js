/*!
 * Facebook React Starter Kit | https://github.com/kriasoft/react-starter-kit
 * Copyright (c) KriaSoft, LLC. All rights reserved. See LICENSE.txt
 */

'use strict';
// Include Gulp and other build automation tools and utilities
// See: https://github.com/gulpjs/gulp/blob/master/docs/API.md
var gulp = require('gulp');
var gutil = require("gulp-util");
var $ = require('gulp-load-plugins')();
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var webpack = require('webpack');
//var jest = require('gulp-jest');

var argv = require('minimist')(process.argv.slice(2));

// Settings
var DEST = './build';                         // The build output folder
var RELEASE = false; //!!argv.release;                 // Minimize and optimize during a build?
var GOOGLE_ANALYTICS_ID = 'UA-XXXXX-X';       // https://www.google.com/analytics/web/
var AUTOPREFIXER_BROWSERS = [                 // https://github.com/ai/autoprefixer
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

var src = {};
var watch = true;
var pkgs = (function() {
  var pkgs = {};
  var map = function(source) {
    for (var key in source) {
      pkgs[key.replace(/[^a-z0-9]/gi, '')] = source[key].substring(1);
    }
  };
  map(require('./package.json').dependencies);
  return pkgs;
}());

// The default task
gulp.task('default', ['build']);

// Build the app from source code
gulp.task('build', function(cb) {
  runSequence(['styles', 'webpack', 'watch'], cb);
});

// CSS style sheets
gulp.task('styles', function() {
  src.styles = 'src/styles/**/*.{css,less}';
  return gulp.src('src/styles/bootstrap.less')
    .pipe($.plumber())
    .pipe($.less({
      sourceMap: !RELEASE,
      sourceMapBasepath: __dirname
    }))
    .on('error', console.error.bind(console))
    .pipe($.autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe($.csscomb())
    .pipe($.if(RELEASE, $.minifyCss()))
    .pipe(gulp.dest(DEST + '/css'))
    .pipe($.size({title: 'styles'}));
});

gulp.task("webpack", function(callback) {
    
    var config = require('./config/webpack.js')(RELEASE);
    // run webpack
    webpack(config, function(err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
            colors: true
        }));
        callback();
    });
});

gulp.task('watch', function() {
    //runSequence(['build']);
    gulp.watch('src/**/*', ['styles', 'webpack']);
});

gulp.task('jest', function() {
    /*
    return gulp.src('./').pipe(jest({
        scriptPreprocessor: "tests/support/preprocessor.js",
        testDirectoryName: "tests",
        unmockedModulePathPatterns: [
            "node_modules/react",
            "node_modules/fluxxor"
        ]
    }));
*/
});

gulp.task('watchtest', function() {
    gulp.watch(['tests/**/*', 'src/**/*'], ['jest']);
}); 

gulp.task('dist', function() {
    RELEASE = true;
    runSequence('styles', 'webpack');
});