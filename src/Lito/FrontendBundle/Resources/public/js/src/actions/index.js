ContentActions = require('./ContentActions');
UserActions = require('./UserActions');

module.exports = {
  content: ContentActions,
  user: UserActions
};