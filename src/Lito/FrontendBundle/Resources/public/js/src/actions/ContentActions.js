var ContentConstants = require('../constants/ContentConstants');

var ContentFetcher = require('../utils/ContentFetcher');

var ContentActions = {
    loadGlobalSettings: function() {
        var self = this;
        self.dispatch(ContentConstants.LOAD_CONTENT_GLOBAL);
        ContentFetcher({
            ident: "type=global"
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_CONTENT_GLOBAL_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_CONTENT_GLOBAL_FAIL);
        });
    },

    loadBaseContent: function(identifiedWith) {
        var self = this;
        var inclUserCategory = identifiedWith ? "&identifier=" + identifiedWith : "";

        self.dispatch(ContentConstants.LOAD_CONTENT_BASE);
        ContentFetcher({
            ident: "type=base" + inclUserCategory    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_CONTENT_BASE_SUCCESS, {
                identifier: identifiedWith,
                data: data
            });
        }, function() {
            self.dispatch(ContentConstants.LOAD_CONTENT_BASE_FAIL);
        });
    },

    loadGalleryContent: function(identifiedWith) {
        var self = this;
        var inclUserCategory = identifiedWith ? "&identifier=" + identifiedWith : "";

        self.dispatch(ContentConstants.LOAD_CONTENT_GALLERY);
        ContentFetcher({
            ident: "type=gallery" + inclUserCategory    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_CONTENT_GALLERY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_CONTENT_GALLERY_FAIL);
        });
    },

    loadAlbums: function(identifiedWith) {
        var self = this;

        self.dispatch(ContentConstants.LOAD_ALBUMS);
        ContentFetcher({
            ident: "type=album"    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_ALBUMS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_ALBUMS_FAIL);
        });
    },

    loadNews: function(identifiedWith) {
        var self = this;

        self.dispatch(ContentConstants.LOAD_NEWS);
        ContentFetcher({
            ident: "type=news"    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_NEWS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_NEWS_FAIL);
        });
    },

    loadEvents: function(identifiedWith) {
        var self = this;

        self.dispatch(ContentConstants.LOAD_EVENTS);
        ContentFetcher({
            ident: "type=event"    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_EVENTS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_EVENTS_FAIL);
        });
    },

    loadCategories: function(userToken) {
        var self = this;
        var inclUserCategory = userToken ? "&related_to_user=" + userToken : "";

        self.dispatch(ContentConstants.LOAD_CONTENT_CATEGORY);
        ContentFetcher({
            ident: "type=category" + inclUserCategory    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_CONTENT_CATEGORY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_CONTENT_CATEGORY_FAIL);
        });
    },

    loadMessages: function(userToken) {
        var self = this;
        var inclUserCategory = userToken ? "&related_to_user=" + userToken : "";

        self.dispatch(ContentConstants.LOAD_CONTENT_MESSAGE);
        ContentFetcher({
            ident: "type=message" + inclUserCategory    
        }, function(data) {
            self.dispatch(ContentConstants.LOAD_CONTENT_MESSAGE_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.LOAD_CONTENT_MESSAGE_FAIL);
        });
    },

    addCategory: function(category, userToken) {
        var self = this;

        self.dispatch(ContentConstants.ADD_CATEGORY, category);
        
        ContentFetcher({
            data: category,
            type: "POST",
            ident: "type=category"    
        }, function(data) {
            self.dispatch(ContentConstants.ADD_CATEGORY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.ADD_CATEGORY_FAIL);
        });
    },

    addEvent: function(event) {
        var self = this;

        self.dispatch(ContentConstants.ADD_EVENT, event);
        
        ContentFetcher({
            data: event,
            type: "POST",
            ident: "type=event"    
        }, function(data) {
            self.dispatch(ContentConstants.ADD_EVENT_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.ADD_EVENT_FAIL);
        });
    },

    addNews: function(news) {
        var self = this;

        self.dispatch(ContentConstants.ADD_NEWS, news);
        
        ContentFetcher({
            data: news,
            type: "POST",
            ident: "type=news"    
        }, function(data) {
            self.dispatch(ContentConstants.ADD_NEWS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.ADD_NEWS_FAIL);
        });
    },

    addAlbum: function(album) {
        var self = this;

        self.dispatch(ContentConstants.ADD_NEWS, album);
        
        ContentFetcher({
            data: album,
            type: "POST",
            ident: "type=album"    
        }, function(data) {
            self.dispatch(ContentConstants.ADD_ALBUM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.ADD_ALBUM_FAIL);
        });
    },

    removeCategory: function(categoryID, userToken) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_CATEGORY, categoryID);
        
        ContentFetcher({
            type: "DELETE",
            ident: "type=category",
            itemId: categoryID
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_CATEGORY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_CATEGORY_FAIL);
        });
    },

    removeEvent: function(eventId) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_EVENT, eventId);
        
        ContentFetcher({
            type: "DELETE",
            ident: "type=event",
            itemId: eventId
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_EVENT_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_EVENT_FAIL);
        });
    },

    removeNews: function(id) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_NEWS, id);
        
        ContentFetcher({
            type: "DELETE",
            ident: "type=news",
            itemId: id
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_NEWS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_NEWS_FAIL);
        });
    },

    removeDocument: function(documentID, userToken) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_DOCUMENT, documentID);
        
        ContentFetcher({
            type: "DELETE",
            ident: "type=document",
            itemId: documentID
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_DOCUMENT_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_DOCUMENT_FAIL);
        });
    },

    removeMessage: function(messageID, userToken) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_MESSAGE, messageID);
        
        ContentFetcher({
            type: "DELETE",
            ident: "type=message",
            itemId: messageID
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_MESSAGE_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_MESSAGE_FAIL);
        });
    },

    editCategory: function(categoryID, data) {
        var self = this;

        self.dispatch(ContentConstants.EDIT_CATEGORY);
        
        ContentFetcher({
            type: "PUT",
            ident: "type=category",
            itemId: categoryID,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_CATEGORY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_CATEGORY_FAIL);
        });
    },

    editEvent: function(id, data) {
        var self = this;
        self.dispatch(ContentConstants.EDIT_EVENT);
        
        ContentFetcher({
            type: "PUT",
            ident: "type=event",
            itemId: id,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_EVENT_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_EVENT_FAIL);
        });
    },

    editNews: function(id, data) {
        var self = this;
        self.dispatch(ContentConstants.EDIT_NEWS);
        
        ContentFetcher({
            type: "PUT",
            ident: "type=news",
            itemId: id,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_NEWS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_NEWS_FAIL);
        });
    },


    editAlbum: function(id, data) {
        var self = this;
        self.dispatch(ContentConstants.EDIT_ALBUM);
        
        ContentFetcher({
            type: "PUT",
            ident: "type=album",
            itemId: id,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_ALBUM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_ALBUM_FAIL);
        });
    },


    editBaseContent: function(id, data, identifier) {
        var self = this;
        
        self.dispatch(ContentConstants.EDIT_CATEGORY);
        
        ContentFetcher({
            type: "PUT",
            ident: "type=base",
            itemId: id,
            data: data,
            identifier: identifier
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_CONTENT_BASE_SUCCESS, {
                identifier: identifier,
                data: data
            });
        }, function() {
            self.dispatch(ContentConstants.EDIT_CONTENT_BASE_FAIL);
        });
    },

    editSettings: function(data) {
        var self = this;

        self.dispatch(ContentConstants.EDIT_SETTINGS);

        ContentFetcher({
            type: "PUT",
            ident: "type=global",
            itemId: 1,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_SETTINGS_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_SETTINGS_FAIL);
        });
    },

    editGalleryItem: function(data) {
        var self = this;

        self.dispatch(ContentConstants.EDIT_GALLERY_ITEM);

        ContentFetcher({
            type: "PUT",
            ident: "type=gallery",
            itemId: data.id,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_GALLERY_ITEM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_GALLERY_ITEM_FAIL);
        });
    },

    addGalleryItem: function(data) {
        var self = this;

        self.dispatch(ContentConstants.ADD_GALLERY_ITEM);

        ContentFetcher({
            type: "POST",
            ident: "type=gallery",
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.ADD_GALLERY_ITEM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.ADD_GALLERY_ITEM_FAIL);
        });
    },

    removeAlbum: function(id) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_ALBUM, id);
                
        ContentFetcher({
            type: "DELETE",
            ident: "type=album",
            itemId: id
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_ALBUM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_ALBUM_FAIL);
        });
    },

    removeGalleryItem: function(id) {
        var self = this;

        self.dispatch(ContentConstants.REMOVE_GALLERY_ITEM, id);
                
        ContentFetcher({
            type: "DELETE",
            ident: "type=gallery",
            itemId: id
        }, function(data) {
            self.dispatch(ContentConstants.REMOVE_GALLERY_ITEM_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.REMOVE_GALLERY_ITEM_FAIL);
        });
    },

    // TBD not used in the current project
    editGalleryContent: function(data) {
        var self = this;

        self.dispatch(ContentConstants.EDIT_GALLERY);

        ContentFetcher({
            type: "PUT",
            ident: "type=gallery",
            itemId: data.id,
            data: data
        }, function(data) {
            self.dispatch(ContentConstants.EDIT_GALLERY_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.EDIT_GALLERY_FAIL);
        });
    },

    uploadDocument: function(doc, category) {
        var self = this;

        self.dispatch(ContentConstants.UPLOAD_DOCUMENT, category);
        
        ContentFetcher({
            upload: true,
            data: doc,
            type: "POST",
            ident: "type=document" + (category? "&category=" + category : "")    
        }, function(data) {
            self.dispatch(ContentConstants.UPLOAD_DOCUMENT_SUCCESS, data);
        }, function() {
            self.dispatch(ContentConstants.UPLOAD_DOCUMENT_FAIL);
        });
    },
};

module.exports = ContentActions;