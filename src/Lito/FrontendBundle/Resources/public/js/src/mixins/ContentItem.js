
var WriteMode = {
  getInitialState: function() {
      return {
        openOverlay: false,
        documentId: null,
        documentName: null,
        categoryName: null,
        imageId: null
      };
    },


   openMedia: function () {
     this.setState({
       openOverlay: true
     });
   },

   handleOnOverlayClose: function () {
     this.setState({
       openOverlay: false
     });
   },

   onBodyChange: function (value) {
     this.setState({
       body: value
     });
   },

   handlePicker: function (document) {
      this.setState({
        openOverlay: false,
        imageId: document.id,
        documentName: document.name,
        categoryName: document.categoryName
      });
    },

   renderOverlay: function (Modal, AdminMedia) {
      return this.state.openOverlay? 
        <Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePicker} />} limited={true} flux={flux} />:
        {};
   },

   renderImage: function (ImageItem) {
    var title = this.state.title || this.props.obj && this.props.obj.title; 
      
      return (this.state.imageId || (this.props.obj && this.props.obj.images && this.props.obj.images[0] && this.props.obj.images[0].documentId))? 
        <ImageItem 
          category={this.state.categoryName || (this.props.obj.images && this.props.obj.images[0] && this.props.obj.images[0].documentCategoryName)} 
          name={this.state.documentName || this.props.obj.images[0].documentName}
          title={title} /> :
        {};
   },

   renderButtons: function () {
   		return this.props.isAddMode ?
       (<div>
        <div className="btn-group"> 
        <button type="submit" className="btn btn-primary" onClick={this.addItem}>Add</button>
        <button type="submit" className="btn btn-default" onClick={this.props.cancelHandler}>Cancel</button> 
        </div>
       </div>):
       (<div>
        <div className="btn-group"> 
        <button type="submit" className="btn btn-primary" onClick={this.updateItem}>Save</button>
        <button type="submit" className="btn btn-default" onClick={this.props.cancelHandler}>Cancel</button>
        </div>
       </div>)
   }


};

module.exports.WriteMode = WriteMode;


var ReadMode = {
  renderImage: function (ImageItem) {
      
      return this.props.obj && this.props.obj.images && this.props.obj.images[0] && this.props.obj.images[0].documentId? 
        <ImageItem 
          category={this.props.obj.images && this.props.obj.images[0] && this.props.obj.images[0].documentCategoryName} 
          name={this.props.obj.images[0].documentName} /> :
        {};
   },

  renderButtons: function () {
    return this.props.isAdmin?
      <div className="form-group clearfix">
          <button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
          <button type="submit" className="btn btn-default" onClick={this.props.removeHandler}>Remove</button>
       </div>:
      {};
  }
};

module.exports.ReadMode = ReadMode;


var BaseContent = {
  getInitialState: function() {
    return {
      isReadMode: true
    };
  },

  getDefaultProps: function () {
    return {
      isAddMode: false,
    };
  },

  activateReadMode: function () {
    this.setState({
      isReadMode: true
    });
  },
  
  editElement: function () {
    this.setState({
      isReadMode: false
    });
  },


  removeItem: function (event) {
    event.preventDefault();
    this.props.onRemove(this.props.obj.id || this.props.obj.documentId);
  },


  renderMode: function (View, Edit, Add) {
    var mode;
    if(!this.state.isReadMode) {

      mode = 
        <Edit 
          isAddMode={this.props.isAddMode}
          cancelHandler={this.activateReadMode}
          onAdd={this.props.onAdd}
          onEdit={this.props.onEdit}
          obj={this.props.obj} />;
      
    }else {

      if(this.props.isAddMode) {
        
      mode = 
        <Add editHandler={this.editElement} />;

      } else {
        
        mode = 
        <View
          obj={this.props.obj} 
          isAdmin={this.props.isAdmin} 
          editHandler={this.editElement} 
          removeHandler={this.removeItem} />;
      }
      
    }

    return mode;

  }

};

module.exports.BaseContent = BaseContent;
