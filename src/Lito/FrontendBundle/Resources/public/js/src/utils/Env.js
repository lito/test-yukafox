
var CONTENT_BASE_URL = "/api";
var CONTENT_JSON_PATH = "/contents";
var AuthClient = require('./AuthClient.js');


exports.getContentURL = function(params, identifier) {
    var id = (typeof identifier != 'undefined')? '/' + identifier: '';
    //var token = (typeof localStorage.authToken!='undefined')? + localStorage.authToken: '';
    var token = localStorage.authToken;
    	
    return CONTENT_BASE_URL + CONTENT_JSON_PATH + id + "?" + params + "&" + 'token=' + token;
};

// possible environments: dev, test, prod
exports.getEnvironment = function() {
	return (window._GLOBALS && window._GLOBALS.environment) || 'prod'; 
}