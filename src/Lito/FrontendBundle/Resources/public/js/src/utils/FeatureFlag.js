var _ = require('underscore');

var Env = require('./Env');

var enabledFeatures = {
	navigation: ['dev']
};

function storeFeatureFlags (featureFlags) {
	window.localStorage.setItem('enabledFeatures', JSON.stringify(featureFlags));
}

if(!window.localStorage.getItem('enabledFeatures')) {
	storeFeatureFlags(enabledFeatures);	
}

exports.getFeatureFlags = function () {
	var persistFlags = window.localStorage.getItem('enabledFeatures');
	if(persistFlags) {
		return JSON.parse(persistFlags);
	}

	return enabledFeatures;
	
};

exports.isEnabled = function (feature, customEnvironment) {
	var environment = customEnvironment || Env.getEnvironment();
	var featureFlags = exports.getFeatureFlags();
	if(!featureFlags[feature]) {
		return true;
	}

	return _.find(featureFlags[feature], function (featureEnvironment) {
		return (featureEnvironment === environment);
	});
	
};

exports.enableFeature = function (feature, environment) {
	var featureFlags = exports.getFeatureFlags();
	if(!featureFlags[feature] || exports.isEnabled(feature, environment)) {
		return true;
	}
	featureFlags[feature].push(environment);
	storeFeatureFlags(featureFlags);
	
	return exports.isEnabled(feature, environment);
};