/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var ContentItem = require('./../mixins/ContentItem.js');

var AdminMedia = require('./Admin/Media.jsx');
var ImageItem = require('./CategoryItem.jsx');
var AddContentItem = require('./AddContentItem.jsx');
var Modal = require('./Modal.jsx');
var EditorWrapper = require('./EditorWrapper.jsx');



var WriteNewsItem = React.createClass({

  mixins: [ContentItem.WriteMode],

  getDefaultProps: function () {
    return {
        basePath: '/uploads/',
      }
  },

  addItem: function (event) {
    event.preventDefault();
    this.props.onAdd({
      title: this.refs.title.getDOMNode().value,
      intro: this.refs.intro.getDOMNode().value,
      body: this.state.body,
      documents: [this.state.imageId],
      images: [this.state.imageId]
    });
    
    this.props.cancelHandler();
  },

  updateItem: function (event) {
    event.preventDefault();
    var obj = this.props.obj;
    obj.title = this.refs.title.getDOMNode().value;
    obj.intro = this.refs.intro.getDOMNode().value;
    obj.body = this.state.body;
    obj.images = [this.state.imageId || obj.image];
    obj.documents = [this.state.imageId || obj.image];

    this.props.onEdit(this.props.obj.id, obj);
    this.props.cancelHandler();
  },

  openMediaForEditor: function () {
     this.setState({
       openOverlayForEditor: true
     });
   },

  handlePickerForEditor: function (document) {
      var path = this.props.basePath + document.categoryName + '/' + document.name;
      this.refs.body.refs.injectinput.getDOMNode().value = path;
      var a = this.refs.body.inject('image');
      a();

      this.handleOnOverlayClose();
    //  $('.modal-backdrop.fade.in').remove();
  },

  render: function () {
    var title, intro, body, openOverlay = this.renderOverlay(Modal, AdminMedia);
    var buttons = this.renderButtons();
    var image = this.renderImage(ImageItem);

    if(!this.props.isAddMode) {
      title = this.props.obj.title,
      intro = this.props.obj.intro,
      body = this.state.body || this.props.obj.body;

    }

    return (
      <div className="news-item clearfix">
        <div className="list-group-item row">
          <div className="news-thumb form-group col-xs-4">
            {image} 
          </div>
          <div className="news-content form-group col-xs-8">
            <div className="row">
              <div className="news-item-title-section form-group col-xs-9">
                <input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
              </div>
            </div>
            <div className="row">
              <div className="news-item-intro-section form-group col-xs-12">
                <input type="text" className="form-control" placeholder="Intro" defaultValue={intro} ref="intro" />
              </div>
            </div>
            <div className="row>">
              <div className="change-image-button form-group col-sm-12"> 
                <button type="submit" className="btn btn-default" onClick={this.openMedia}>Change image</button>  
                  {openOverlay}
              </div>
            </div>
            <div className="row>">
              <div className="change-image-button form-group col-sm-12"> 
                <EditorWrapper content={body} onChange={this.onBodyChange} />
              </div>
            </div>
            <div className="row">
              <div className="edit-buttons form-group col-sm-12">
                <div className="btn-group"> 
                  {buttons}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var Router = require('react-router'),
    Navigation = Router.Navigation;

var ReadNewsItem = React.createClass({

  mixins: [Navigation, ContentItem.ReadMode],

  linkTo: function () {
    if(this.props.obj.routingName) {
      // TBD
      this.replaceWith("/news/" + this.props.obj.routingName);
    }
  }, 
  
  render: function () {
    var image = this.renderImage(ImageItem), 
    bodyText, 
    adminButtons = this.renderButtons(),
    linkStart, linkEnd;
    
    if(this.props.isDetails) {
      bodyText = 
        (<div className="form-group col-xs-4">
          {this.props.obj.body}
        </div>);
    }

    

    return (
      <div className="news-item clearfix">
        <div className="row">
          <div className="news-date form-group col-xs-12">
            {this.props.obj.date.split(" ")[0]}
          </div>
        </div>
        <div className="row linkArea" onClick={this.linkTo}>

          <div className="news-thumb form-group col-xs-4">
            {image}
          </div>
          <div className="news-content form-group col-xs-8">
            <div className="row">
              <div className="news-item-title-section form-group col-xs-12">
                <p className="news-item-title">{this.props.obj.title}</p>
              </div>
            </div>
            <div className="row">
              <div className="news-item-intro-section form-group col-xs-12">
                {this.props.obj.intro}
              </div>
            </div>
          </div>
          {bodyText}
        </div>

        {adminButtons}

      </div>
    );
  }
});

var BaseContent = React.createClass({

  mixins: [ContentItem.BaseContent],

  render: function () {
    var newsMode = this.renderMode(ReadNewsItem, WriteNewsItem, AddContentItem);

    return (
      <div className="">{newsMode}</div>
    );
  }
});


module.exports = BaseContent;