/** @jsx React.DOM */
var _ = require('underscore');

var React = require("react");
var Router = require('react-router'),
    Navigation = Router.Navigation,
    Link = Router.Link;

var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var FeatureFlag = require("../utils/FeatureFlag");

var LogoItem = React.createClass({

    getDefaultProps: function () {
      return {
        basePath: '/uploads/',
        fullPath: null,
        flux: flux
      };
    },

    render: function() {
      var path = this.props.basePath + this.props.category + "/" + this.props.name;
      return (
        <div className="logo">
          <Link to="start"><img className="logo-item" src={path} /></Link>
        </div>
      );

    }

});

var Header = React.createClass({

    render: function () {
      var logo = (this.props.logoCategoryName && this.props.logoDocumentName ) ? 
        <LogoItem category={this.props.logoCategoryName} name={this.props.logoDocumentName} /> :
        null;

      var navigation = FeatureFlag.isEnabled('navigation') ? 
        <nav>
          <ul className="top-nav">
            <li><Link to="/lookbook">Lookbook</Link></li>
            <li><Link to="/news">News</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/blog">Blog</Link></li>
          </ul>
        </nav> :
        null;

      return (
        <header className="header">
          <div className="header-content row">
            <div className="col-sm-4">
              {logo}
            </div>
            <div className="navbar-header col-sm-8">
              {navigation}
            </div>
          </div>
        </header>
      );
    }
});

module.exports = Header