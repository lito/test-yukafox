/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var ContentItem = require('./BaseContent.jsx');

var BaseContentList = React.createClass({

  render: function () {
    var list = [], self = this;
    var Element = this.props.element || ContentItem;

    _.each(this.props.list, function (obj) {
      list.push(<Element obj={obj} isAdmin={self.props.isAdmin} onEdit={self.props.onEdit} onRemove={self.props.onRemove} />);
    });
    if(this.props.isAdmin) {
      list.push(<Element isAddMode={true} isAdmin={true} onAdd={this.props.onAdd} />);  
    }
    
  
    return (
      <div className="contentList">
        {list}
      </div>
    )
    ;
  }
});


module.exports = BaseContentList;