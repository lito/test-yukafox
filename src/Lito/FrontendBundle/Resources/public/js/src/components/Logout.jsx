/** @jsx React.DOM */

var React = require("react"),
  Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var Logout = React.createClass({
  mixins: [FluxMixin],

  componentDidMount: function() {
    this.getFlux().actions.user.logoutUser(); 
  },

  render: function() {
    return (
    <div>
      <p>You are now logged out</p>
    </div>
      
    );
  }
});

module.exports = Logout;