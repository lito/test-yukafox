var React = require("react"),
    Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

 
var Router = require('react-router');
var { Route, RouteHandler, Link, Navigation } = Router;

var ThumbnailItem = require('./../CategoryItem.jsx');

var UploadImageToCategory = React.createClass({
      
      mixins: [FluxMixin, StoreWatchMixin("CategoryStore")],

       getStateFromFlux: function() {
        var store = this.getFlux().store("CategoryStore");
        return {
          uploading: store.uploadingDocument,
          error: store.uploadingError
        };
      },

      uploadFile: function (event) {
        event.preventDefault();

        var formData = new FormData($('#file-form')[0]);
        var button = $('#upload-button');
    
        this.getFlux().actions.content.uploadDocument(formData, this.props.category);
      },

      render: function(){
        uploadBtn = {
          className: "btn btn-primary",
          text: "Upload",
        }

        if(this.state.uploading) {
          uploadBtn.className = "btn btn-default";
          uploadBtn.text = "Uploading ...";
        }

        return (
            
            <div id="upload-to-category" className="clearfix">
              <form id="file-form" className="form-inline well" method="POST" enctype="multipart/form-data" onSubmit={this.uploadFile} >
                <h4>Upload a new document</h4>

              <div className="form-group">
                <input id="file-select" name="file" type="file" />
                <p className="help-block">Max size 2MB</p>
              </div>
                <div className="form-group">
                  <button type="submit" id="upload-button" className={uploadBtn.className}>{uploadBtn.text}</button>
                </div>
                  
              </form>
            </div>
              
        )
      }
});

var DocumentItem = React.createClass({
      
      mixins: [FluxMixin],

      removeItem: function (event) {
        event.preventDefault();
        
        this.getFlux().actions.content.removeDocument(this.props.id);
      },

      handlePicker: function (event) {
        event.preventDefault();
        this.props.handlePicker({
          id: this.props.id,
          categoryName: this.props.category.name,
          name: this.props.name
        });
      },

      render: function(){
        var name = this.props.category.name,
            title = this.props.category.title;

        var picker = this.props.handlePicker?
            <div className="form-group col-xs-1">
              <div className="btn-group"> 
                <button type="submit" className="btn btn-primare" onClick={this.handlePicker}>Select</button>
              </div>
            </div>:
            null;

        return (
          <li className="list-group-item form-inline row">
            <ThumbnailItem category={name} name={this.props.name} title={this.props.title} />

            <div className="form-group col-xs-6">
              {this.props.name}
            </div>
            {picker}
            <div className="form-group col-xs-1">
              <div className="btn-group"> 
                <button type="submit" className="btn btn-default" onClick={this.removeItem}>Remove</button>
              </div>
            </div>
          </li>
                
        )
      }
});

var DocumentList = React.createClass({
      getInitialState: function () {
        return {
          categoryName: this.props.select[0].props.value
        };
      },

      getDefaultProps: function () {
        return {
          limited: false
        };
      },
      
      handleChange: function () {
        var element = this.refs.changeFolder.getDOMNode();
        this.setState({
          categoryName: _.findWhere(this.props.categories, {name: element.value}).name
        });

      },

      render: function(){
        var self = this;
        var currentCategory = _.findWhere(this.props.categories, {name: this.state.categoryName}); 
        
        var DocumentList = _.map(currentCategory.documents, function(obj, i) {
          return (
            <DocumentItem category={_.findWhere(self.props.categories, {name: currentCategory.name})} name={obj.name} title={obj.title} id={obj.id} handlePicker={self.props.handlePicker} />
            );
        });

        var Upload = (
            <UploadImageToCategory category={currentCategory.id} />
        );

        var header = this.props.limited? 
              (<div>
              <form className="form-inline">
                <div className="form-group">
                  <label className="control-label">Select a folder: </label>
                  <select 
                    ref="changeFolder"
                    className="form-control"
                    onChange={this.handleChange} >
                    {this.props.select}
                  </select>
                </div>
                
              </form></div>) :
              {};

        return (
            <div id="category-list" className="category-list clearfix">
                {header}
                <ul className="list-group">
                  {DocumentList}
                </ul>
                {Upload}
            </div>
              
        )


      }
});

var AdminMedia = React.createClass({
      mixins: [FluxMixin, StoreWatchMixin("CategoryStore")],

      getStateFromFlux: function() {
        var store = this.getFlux().store("CategoryStore");
        return {
          loading: store.loading,
          error: store.error,
          categories: store.categories
        };
      },

      render: function(){
          var FolderList = [], 
              Content;


          if(this.state.categories) {

            _.each(this.state.categories, function(obj) {
              var Element = <option value={obj.name}>{obj.title}</option>;
              if(!obj.show_in_menu) {
                FolderList.unshift(Element);
              }else {
                FolderList.push(Element);  
              }
              
            });

            Content = <DocumentList categories={this.state.categories} select={FolderList} handlePicker={this.props.handlePicker} />;

          } 
          return (
              <div className="AdminCategories">
                  <h2>Media</h2>
                  {Content}
              </div>
          );

      }

});

module.exports = AdminMedia; 