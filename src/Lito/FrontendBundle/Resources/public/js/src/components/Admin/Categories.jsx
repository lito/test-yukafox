var React = require("react"),
    Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

 
var Router = require('react-router');
var { Route, RouteHandler, Link, Navigation } = Router;

var CategoryItem = require('./../CategoryItem.jsx');

var NewCategory = React.createClass({
      
      mixins: [FluxMixin],

      getInitialState: function() {
        return {
          inActive: true
        };
      },

      addItem: function (event) {
        var data = {
          name: this.refs.name.getDOMNode().value,
          title: this.refs.title.getDOMNode().value,
          relatedToUser: 0,
          showInMenu: 1 //
        };
        event.preventDefault();
        
        this.getFlux().actions.content.addCategory(data);
      },

      activateMode: function (event) {
        event.preventDefault();
        this.setState({
          inActive: this.state.inActive? false : true
        });
        
      },

      render: function(){
          var mode = this.state.inActive?
                <div className="form-group pull-left">
                  <button type="submit" className="btn btn-primary" onClick={this.activateMode}>Add a new Category</button>
                </div>:
                <div className="form-inline row">
                  <div className="form-group pull-left col-sm-6">
                    <input className="form-control" ref="name" name="name" type="text" placeholder="Name" />
                    <input className="form-control" ref="title" name="title" type="text" placeholder="Title" />
                  </div>
                  <div className="form-group pull-right col-sm-6">
                    <div className="btn-group"> 
                      <button type="submit" className="btn btn-primary" onClick={this.addItem}>Submit</button>
                      <button type="submit" className="btn btn-default" onClick={this.activateMode}>Cancel</button>
                    </div>
                  </div>
                </div>

        return (
            <div id="add-category">
              {mode}
            </div>
              
        )
      }
});


var UploadImageToCategory = React.createClass({
      
      mixins: [FluxMixin],

      uploadFile: function (event) {
        event.preventDefault();

        var formData = new FormData($('#file-form')[0]);
        var button = $('#upload-button');
        var category = $('#category-select').val()? "&category=" + $('#category-select').val() : "";
        
        button.html("Uploading");
        

        $.ajax({
            type: "POST",
            data: formData,
            processData: false, 
            contentType: false, 
            url: "api/contents.json?type=document" + category,
            cache: false,
            success: function (result) {
                button.html("Upload");
            },
            error: function (error) {
                
            }
        });
      },

      render: function(){

        return (
            
            <div id="upload-to-category" className="clearfix">
              <form id="file-form" className="form-inline well" method="POST" enctype="multipart/form-data" onSubmit={this.uploadFile} >
                <h4>Upload image</h4>

              <div className="form-group">
                <label for="file-select">File input</label>
                <input id="file-select" name="file" type="file" />
                <p className="help-block">Max size 2MB</p>
              </div>
                <div className="form-group">
                  <button type="submit" id="upload-button" className="btn btn-default">Upload</button>
                </div>
                  
              </form>
            </div>
              
        )
      }
});

var CategoryItem = React.createClass({
      
      mixins: [FluxMixin],

      removeItem: function (event) {
        event.preventDefault();
        
        this.getFlux().actions.content.removeCategory(this.props.id);
      },

      updateItem: function (event) {
        event.preventDefault();
        var obj = this.props.category;
        obj.title = this.refs.title.getDOMNode().value;

        this.getFlux().actions.content.editCategory(this.props.id, obj);
      },


      render: function(){
        var name = this.props.category.name,
            title = this.props.category.title;
        return (
          <li className="list-group-item form-inline row">
            <div className="form-group col-xs-4">
              <p className="form-control-static" name="category-name">{name}</p>
            </div>
            <div className="form-group col-xs-5">
              <input type="text" className="form-control" placeholder={title} ref="title" />
            </div>
            <div className="form-group col-xs-3">
              <div className="btn-group"> 
                <button type="submit" className="btn btn-default" onClick={this.updateItem}>Save</button>
                <button type="submit" className="btn btn-default" onClick={this.removeItem}>Remove</button>
              </div>
            </div>
          </li>
                
        )
      }
});

var CategoryList = React.createClass({
      
      render: function(){
        var list = _.map(this.props.categories, function(obj) {
            return <CategoryItem category={obj} id={obj.id} />
        });


        return (
            <div id="category-list" className="category-list clearfix">
                <h4>List of existing categories</h4>
                <ul className="list-group">
                  {list}
                </ul>
            </div>
              
        )
      }
});

var AdminCategories = React.createClass({
      mixins: [FluxMixin, StoreWatchMixin("CategoryStore")],

      getStateFromFlux: function() {
        var store = this.getFlux().store("CategoryStore");
        return {
          loading: store.loading,
          error: store.error,
          categories: store.categories
        };
      },

      render: function(){
          
          return (
              <div className="AdminCategories">
                  <h2>Categories</h2>

                    <CategoryList categories={this.state.categories}/>
                    <NewCategory />

              </div>
          );

      }

});

module.exports = AdminCategories; 