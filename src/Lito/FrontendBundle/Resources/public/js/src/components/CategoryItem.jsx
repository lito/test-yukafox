/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');
var Router = require('react-router');
var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;


var CategoryItem = React.createClass({

    getDefaultProps: function () {
      return {
        basePath: '/uploads/',
        fullPath: null,
        flux: flux,
        title: null
      };
    },

    anchorClicked: function(event) {
      event.preventDefault();
      
      blueimp.Gallery($('a.thumbnail'), $('#blueimp-gallery').data());

    },

    render: function() {
      var path = this.props.basePath + this.props.category + "/" + this.props.name;
      return (<div className="gallery-item"><a className="thumbnail" onClick={this.anchorClicked}  data-gallery="data-gallery" title={this.props.title}  href={path}><img className="img-thumb-gallery" src={path} alt={this.props.title} /></a></div>);

    }

});

module.exports = CategoryItem;