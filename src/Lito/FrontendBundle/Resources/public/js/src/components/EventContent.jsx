/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var ContentItem = require('./../mixins/ContentItem.js');

var AdminMedia = require('./Admin/Media.jsx');
var ImageItem = require('./CategoryItem.jsx');
var AddContentItem = require('./AddContentItem.jsx');
var Modal = require('./Modal.jsx');



var WriteEventItem = React.createClass({

  mixins: [ContentItem.WriteMode],

  addItem: function (event) {
    event.preventDefault();
    this.props.onAdd({
      title: this.refs.title.getDOMNode().value,
      intro: this.refs.intro.getDOMNode().value,
      body: "",
      date: this.refs.date.getDOMNode().value

    });
    
    this.props.cancelHandler();
  },

  updateItem: function (event) {
    event.preventDefault();
    var obj = this.props.obj;
    obj.title = this.refs.title.getDOMNode().value;
    obj.intro = this.refs.intro.getDOMNode().value;
    obj.date = this.refs.date.getDOMNode().value;
    this.props.onEdit(this.props.obj.id, obj);
    this.props.cancelHandler();
  },

  render: function () {
    var title, intro, body, date;

    var buttons = this.renderButtons();

    if(!this.props.isAddMode) {
      title = this.props.obj.title,
      intro = this.props.obj.intro;
      body = this.props.obj.body;
      date = this.props.obj.date.split(" ")[0];

    }

    return (
      <div className="events-item clearfix">
        <div className="list-group-item row">
          
          <div className="events-content form-group col-xs-8">
            <div className="row">
              <div className="events-item-title-section form-group col-xs-12">
                <input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
              </div>
            </div>
            <div className="row">
              <div className="events-item-intro-section form-group col-xs-12">
                <input type="text" className="form-control" placeholder="Intro" defaultValue={intro} ref="intro" />
              </div>
            </div>
            <div className="row">
              <div className="events-item-date-section form-group col-xs-12">
                <input type="text" className="form-control" placeholder="Date (dd-mm-yyyy)" defaultValue={date} ref="date" />
              </div>
            </div>
            <div className="row">
              <div className="edit-buttons form-group col-sm-12">
                <div className="btn-group"> 
                  {buttons}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});


var ReadEventItem = React.createClass({

  getDefaultProps: function () {
    return {
      isLimited: false
    };
  },

  mixins: [ContentItem.ReadMode],
  
  render: function () {
    var intro, bodyText, 
    adminButtons = this.renderButtons();
    
    if(!this.props.isLimited) {
      bodyText = 
        (<div className="row">
          <div className="form-group col-xs-4">
          {this.props.obj.body}
          </div>
        </div>);
    }

    return (
      <div className="events-item clearfix">
        <div className="row">
          <div className="events-date form-group col-xs-12">
            {this.props.obj.date.split(" ")[0]}
          </div>
        </div>
        <div className="row">
          <div className="events-content form-group col-xs-8">
            <div className="row">
              <div className="events-item-title-section form-group col-xs-12">
                <p className="events-item-title">{this.props.obj.title}</p>
              </div>
            </div>
            <div className="row">
              <div className="events-item-intro-section form-group col-xs-12">
                 {this.props.obj.intro}
              </div>
            </div>
            {bodyText}
          </div>
         
        </div>

        {adminButtons}

      </div>
    );
  }
});

var EventContent = React.createClass({

  mixins: [ContentItem.BaseContent],

  render: function () {
    var contentMode = this.renderMode(ReadEventItem, WriteEventItem, AddContentItem);

    return (
      <div className="eventItem">{contentMode}</div>
    );
  }
});


module.exports = EventContent;