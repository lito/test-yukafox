/** @jsx React.DOM */

var Fluxxor = require("fluxxor");

var Router = require('react-router'),
    Route = Router.Route,
    Routes = Router.Routes,
    Redirect = Router.Redirect,
    DefaultRoute = Router.DefaultRoute,
    NotFoundRoute = Router.NotFoundRoute;

var stores = require('./stores/index'),
    actions = require('./actions/index');

var flux = new Fluxxor.Flux(stores, actions);
window.flux = flux;

flux.on("dispatch", function(type, payload) {
  if (console && console.log) {
    console.log("[Dispatch]", type, payload);
  }
});

var Application = require('./components/Application.jsx'),
    Login = require('./pages/Login.jsx'),
    Index = require('./pages/Index.jsx'),
    News = require('./pages/News.jsx'),
    NewsDetails = require('./pages/NewsDetails.jsx'),
    AboutPage = require('./pages/About.jsx'),
    LookBookPage = require('./pages/LookBook.jsx'),
    Impressum = require('./pages/Impressum.jsx'),
    Datenschutz = require('./pages/Datenschutz.jsx'),
    Logout = require('./pages/Login.jsx'),
    Admin = require('./pages/Admin.jsx'),
    AdminSettings = require('./components/Admin/Settings.jsx'),
    AdminCategories = require('./components/Admin/Categories.jsx'),
    AdminСustomers = require('./components/Admin/Customers.jsx'),
    AdminMedia = require('./components/Admin/Media.jsx'),
    AdminMessages = require('./components/Admin/Messages.jsx');

var Router = (
  
    <Route handler={Application} >
      <DefaultRoute name="start" handler={Index} />
      <Route name="news" handler={News} />
      <Route name="newsDetails" path="news/:routingName" handler={NewsDetails} />
      <Route name="about" path="about" handler={AboutPage} />
      <Route name="lookbook" path="lookbook" handler={LookBookPage} />
      <Route name="impressum" path="impressum" handler={Impressum} />
      <Route name="datenschutz" path="datenschutz" handler={Datenschutz} />
      <Route name="login" path="login" handler={Login} />
      <Route name="logout" path="logout" handler={Login} />
      <Route name="admin" path="admin" handler={Admin} >
        <Route name="settings" handler={AdminSettings} />
        <Route name="categories" handler={AdminCategories} />
        <Route name="customers" handler={AdminСustomers} />
        <Route name="media" handler={AdminMedia} />
        <Route name="messages" handler={AdminMessages} />
      </Route>
      <NotFoundRoute handler={Index}/>
    </Route>
  
);


module.exports = Router;

