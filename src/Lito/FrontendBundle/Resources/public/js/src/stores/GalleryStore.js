var _ = require('underscore');

var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var GalleryStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.adding = false;
    this.error = false;
    this.gallery = null;

    this.bindActions(
      ContentConstants.LOAD_CONTENT_GALLERY, this.onLoad,
      ContentConstants.LOAD_CONTENT_GALLERY_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_GALLERY_FAIL, this.onLoadFail,

      ContentConstants.EDIT_GALLERY, this.onEdit,
      ContentConstants.EDIT_GALLERY_SUCCESS, this.onEditSuccess,
      ContentConstants.EDIT_GALLERY_FAIL, this.onEditFail,

      ContentConstants.EDIT_GALLERY_ITEM, this.onEditItem,
      ContentConstants.EDIT_GALLERY_ITEM_SUCCESS, this.onEditItemSuccess,
      ContentConstants.EDIT_GALLERY_ITEM_FAIL, this.onEditItemFail,

      ContentConstants.EDIT_GALLERY_ITEM, this.onEditItem,
      ContentConstants.EDIT_GALLERY_ITEM_SUCCESS, this.onEditItemSuccess,
      ContentConstants.EDIT_GALLERY_ITEM_FAIL, this.onEditItemFail,

      ContentConstants.ADD_GALLERY_ITEM, this.onAddItem,
      ContentConstants.ADD_GALLERY_ITEM_SUCCESS, this.onAddItemSuccess,
      ContentConstants.ADD_GALLERY_ITEM_FAIL, this.onAddItemFail,

      ContentConstants.REMOVE_GALLERY_ITEM, this.onRemoveItem,
      ContentConstants.REMOVE_GALLERY_ITEM_SUCCESS, this.onRemoveItemSuccess,
      ContentConstants.REMOVE_GALLERY_ITEM_FAIL, this.onRemoveItemFail

    );
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(data) {
    this.loading = false;
    this.gallery = data;
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEdit: function(data) {
    this.loading = false;
    this.emit("change");
  },

  onEditItem: function(data) {
    this.loading = true;
    this.emit("change");
  },

  onEditSuccess: function(data) {
    this.loading = false;
    this.gallery = data;
    this.emit("change");
  },

  onEditItemSuccess: function(data) {
    this.loading = false;
    this.gallery = _.map(this.gallery, function (obj) {
      if(data.id === obj.id){
  
        obj = data;
      }
      return obj;
    });

    this.emit("change");
  },

  onEditFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEditItemFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onAddItem: function(data) {
    this.loading = true;
    this.emit("change");
  },

  onAddItemSuccess: function(data) {
    this.loading = false;
    this.gallery.push(data);

    this.emit("change");
  },

  onAddItemFail: function(data) {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onRemoveItem: function(id) {
    this.loading = false;
    this.removingPending = id;
    this.emit("change");
  },

  onRemoveItemSuccess: function(data) {
    this.loading = false;
    this.gallery = _.without(this.gallery, _.findWhere(this.gallery, {id: this.removingPending}));
    this.emit("change");
  },

  onRemoveItemFail: function() {
    this.loading = false;
    this.error = true;
    this.removingPending = null;
    this.emit("change");
  }

});

module.exports = GalleryStore;