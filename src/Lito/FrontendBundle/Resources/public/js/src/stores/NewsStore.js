var _ = require('underscore');

var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var NewsStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.news = [];
    this.events = [];

    this.bindActions(
      ContentConstants.LOAD_NEWS, this.onLoadNews,
      ContentConstants.LOAD_NEWS_SUCCESS, this.onLoadNewsSuccess,
      ContentConstants.LOAD_NEWS_FAIL, this.onLoadNewsFail,

      ContentConstants.LOAD_EVENTS, this.onLoadEvents,
      ContentConstants.LOAD_EVENTS_SUCCESS, this.onLoadEventsSuccess,
      ContentConstants.LOAD_EVENTS_FAIL, this.onLoadEventsFail,

      ContentConstants.ADD_NEWS, this.onAddNews,
      ContentConstants.ADD_NEWS_SUCCESS, this.onAddNewsSuccess,
      ContentConstants.ADD_NEWS_FAIL, this.onAddNewsFail,

      ContentConstants.ADD_EVENT, this.onAddEvent,
      ContentConstants.ADD_EVENT_SUCCESS, this.onAddEventSuccess,
      ContentConstants.ADD_EVENT_FAIL, this.onAddEventFail,

      ContentConstants.REMOVE_NEWS, this.onRemoveNews,
      ContentConstants.REMOVE_NEWS_SUCCESS, this.onRemoveNewsSuccess,
      ContentConstants.REMOVE_NEWS_FAIL, this.onRemoveNewsFail,

      ContentConstants.REMOVE_EVENT, this.onRemoveEvent,
      ContentConstants.REMOVE_EVENT_SUCCESS, this.onRemoveEventSuccess,
      ContentConstants.REMOVE_EVENT_FAIL, this.onRemoveNewsFail,

      ContentConstants.EDIT_NEWS, this.onEditNews,
      ContentConstants.EDIT_NEWS_SUCCESS, this.onEditNewsSuccess,
      ContentConstants.EDIT_NEWS_FAIL, this.onEditNewsFail,

      ContentConstants.EDIT_EVENT, this.onEditEvent,
      ContentConstants.EDIT_EVENT_SUCCESS, this.onEditEventSuccess,
      ContentConstants.EDIT_EVENT_FAIL, this.onEditEventFail
    );
  },

  onLoadNews: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadNewsSuccess: function(data) {
    this.loading = false;
    // TBD
    this.news = data;
    this.emit("change");
  },

  onLoadNewsFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onLoadEvents: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadEventsSuccess: function(data) {
    this.loading = false;
    // TBD
    this.events = data;
    this.emit("change");
  },

  onLoadEventsFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onAddNews: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onAddNewsSuccess: function(data) {
    this.loading = false;
    this.news.push(data);
    this.emit("change");
  },

  onAddNewsFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onRemoveNews: function(id) {
    this.loading = true;
    this.removingNewsPending = id;
    this.error = null;
    this.emit("change");
  },

  onRemoveNewsSuccess: function(data) {
    this.loading = false;
    this.news =  _.without(this.news, _.findWhere(this.news, {id: this.removingNewsPending}));
    this.emit("change");
  },

  onRemoveNewsFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEditNews: function(data) {
    this.loading = true;
    this.emit("change");
  },

  onEditNewsSuccess: function(data) {
    this.loading = false;
    this.news = _.map(this.news, function (obj) {
      if(data.id === obj.id){
  
        obj = data;
      }
      return obj;
    });

    this.emit("change");
  },

  onEditNewsFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

   onLoadEvent: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadEventSuccess: function(data) {
    this.loading = false;
    // TBD
    this.events = data;
    this.emit("change");
  },

  onLoadEventFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onAddEvent: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onAddEventSuccess: function(data) {
    this.loading = false;
    this.events.push(data);
    this.emit("change");
  },

  onAddEventFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onRemoveEvent: function(id) {
    this.loading = true;
    this.removingEventPending = id;
    this.error = null;
    this.emit("change");
  },

  onRemoveEventSuccess: function(data) {
    this.loading = false;
    this.events =  _.without(this.events, _.findWhere(this.events, {id: this.removingEventPending}));
    this.emit("change");
  },

  onRemoveEventFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEditEvent: function(data) {
    this.loading = true;
    this.emit("change");
  },

  onEditEventSuccess: function(data) {
    this.loading = false;
    this.events = _.map(this.events, function (obj) {
      if(data.id === obj.id){
  
        obj = data;
      }
      return obj;
    });

    this.emit("change");
  },

  onEditEventFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  }
});

module.exports = NewsStore;