/**
 * @jsx React.DOM
 */

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	React = require('react'),
	Fluxxor = require("fluxxor"),
	FluxMixin = Fluxxor.FluxMixin(React),
	StoreWatchMixin = Fluxxor.StoreWatchMixin,
	Router = require('react-router'),
	Navigation = Router.Navigation;


var ContentList = require('../components/BaseContentList.jsx'),
	EventContent = require('../components/EventContent.jsx'),
	ContactPerson = require('../components/ContactPerson.jsx');

var NewsDetailsPage = React.createClass({
	mixins: [Router.State, FluxMixin, StoreWatchMixin("NewsStore")],

	getDefaultProps: function () {
		return {
			title: "News Details:",
		};
	},
	
	componentDidMount: function () {
			this.getFlux().actions.content.loadNews();
			this.getFlux().actions.content.loadEvents();
		
		
	},

	getStateFromFlux: function() {
		var newsAndEventsStore = this.getFlux().store("NewsStore");
		
		return {
			 news: newsAndEventsStore.news,
			 events: newsAndEventsStore.events
		};
	},

	renderEventList: function (list) {

		var events = 
		<ContentList 
			list={list}  
			isAdmin={this.props.isAdmin} 
			onAdd={this.getFlux().actions.content.addEvent}
			onEdit={this.getFlux().actions.content.editEvent}
			onRemove={this.getFlux().actions.content.removeEvent}
			element={EventContent}  />;

		return (
			<div className="events-block col-sm-4">
				<h2 className="page-title">Next events</h2>
				<div className="list-group-item row">
					{events}
				</div>
			</div>
		);
	},

	renderNewsList: function (list) {
		var newsList = 
			<ContentList 
				list={list} 
				isAdmin={this.props.isAdmin} 
				onAdd={this.getFlux().actions.content.addNews}
				onEdit={this.getFlux().actions.content.editNews}
				onRemove={this.getFlux().actions.content.removeNews} />;

		return (
			<div>
				<h2 className="page-title">Latest news</h2>
				<div className="list-group-item row">
					{newsList}
				</div>

			</div>
			
		);
	},

	renderNews: function () {
		var currentNews = _.findWhere(this.state.news, {routingName: this.getParams().routingName}),
			newsList;

		if(!currentNews) return null;

		newsList = this.renderNewsList(_.filter(this.state.news, function (obj) {
					return obj.id !== currentNews.id
				}));

		return (
			<div className="news-block col-sm-8">
				<h1>{currentNews.title}</h1>
				<div className="news-date">{currentNews.date.split(" ")[0]}</div>
				<div className="intro">
					{currentNews.intro}
				</div>
				<div className="body" dangerouslySetInnerHTML={{__html: currentNews.body}} />

				{newsList}
			</div>
		);
	},
 
	render: function () {
		var news, events, eventList;

		if(this.state.news) {
			news = this.renderNews();
		}

		if(this.state.events) {
			events = this.renderEventList(this.state.events);
		}
		
		return (
			<div className="baseLayout clearfix">
				<div className="centered">
					{news}
					{events}
					<div className="contact-person-section col-sm-4">
						<ContactPerson isAdmin={this.props.isAdmin} />
					</div>
				</div>
			</div>
		);
	}
});

module.exports = NewsDetailsPage;