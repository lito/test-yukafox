/**
 * @jsx React.DOM
 */

'use strict';

var $ = require('jquery');
window.jQuery = require('jquery');

var React = require('react'),
	Fluxxor = require("fluxxor"),
	FluxMixin = Fluxxor.FluxMixin(React),
	StoreWatchMixin = Fluxxor.StoreWatchMixin,
	Router = require('react-router'),
	Navigation = Router.Navigation,
	Carousel = require('react-bootstrap').Carousel,
	CarouselItem  = require('react-bootstrap').CarouselItem,
	Link = Router.Link;

var AdminMedia = require('./../components/Admin/Media.jsx');
var ImageItem = require('./../components/CategoryItem.jsx');
var Modal = require('./../components/Modal.jsx');

var GalleryEditItem = React.createClass({
			
			mixins: [FluxMixin],

			getInitialState: function() {
				return {
					openOverlay: false,
					documentId: document.id,
					documentName: null,
					categoryName: null
				};
			},

			openMedia: function () {
					this.setState({
						openOverlay: true
					});
			},

			handleOnOverlayClose: function () {
				this.setState({
						openOverlay: false
					});
				$(".section").removeClass("active");
			},

			handlePicker: function (document) {
				this.setState({
						openOverlay: false,
						imageId: document.id,
						documentName: document.name,
						categoryName: document.categoryName
					});
			},

			removeItem: function (event) {
				event.preventDefault();
				this.getFlux().actions.content.removeGalleryItem(this.props.obj.id);
			},

			addItem: function (event) {
				event.preventDefault();
				
				this.getFlux().actions.content.addGalleryItem({
					headline: this.refs.headline.getDOMNode().value,
					subheadline: this.refs.subHeadline.getDOMNode().value,
					buttonText: this.refs.buttonText.getDOMNode().value,
					buttonLink: this.refs.buttonLink.getDOMNode().value,
					image: this.state.imageId,
					// TBD
					identifiedWith: "homeStage"
				});

				this.props.closeAddItem();
			},

			updateItem: function (event) {
				event.preventDefault();
				var obj = this.props.obj;
				obj.headline = this.refs.headline.getDOMNode().value;
				obj.subheadline = this.refs.subHeadline.getDOMNode().value;
				obj.buttonText = this.refs.buttonText.getDOMNode().value;
				obj.buttonLink = this.refs.buttonLink.getDOMNode().value;
				obj.image = this.state.imageId || obj.image;
				this.getFlux().actions.content.editGalleryItem(obj);
			},


			render: function(){
				var subHeadline = (this.props.obj && this.props.obj.subheadline) || null,
						headline = (this.props.obj && this.props.obj.headline) || null,
						buttonText = (this.props.obj && this.props.obj.buttonText) || null,
						buttonLink = (this.props.obj && this.props.obj.buttonLink) || null,
						imageId = (this.state.imageId || (this.props.obj && this.props.obj.image && this.props.obj.image.id));
						
						
						var image = imageId? 
							<ImageItem 
								category={this.state.categoryName || (this.props.obj.image && this.props.obj.image.category.name)} 
								name={this.state.documentName || this.props.obj.image.name} /> :
							{},
						openOverlay = this.state.openOverlay? 
						<Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePicker} />} limited={true} flux={flux} />:
						{};

						var buttons = this.props.addMode ?
						 (<div>
							<div className="btn-group"> 
							<button type="submit" className="btn btn-primary" onClick={this.addItem}>Add</button>
							<button type="submit" className="btn btn-default" onClick={this.props.closeAddItem}>Cancel</button> 
							</div>
						 </div>):
						 (<div>
							<div className="btn-group"> 
							<button type="submit" className="btn btn-primary" onClick={this.updateItem}>Save</button>
							<button type="submit" className="btn btn-default" onClick={this.removeItem}>Remove</button>
							</div>
						 </div>);
				return (
					<div className="list-group-item row">
						<div className="slide-image form-group col-xs-4">
							{image} 
						</div>
						<div className="slide-headline form-group col-xs-4">
							<input type="text" className="form-control" placeholder="Headline" defaultValue={headline} ref="headline" />
						</div>
						<div className="slide-subheadline form-group col-xs-4">
							<input type="text" className="form-control" placeholder="Subheadline" defaultValue={subHeadline} ref="subHeadline" />
						</div>
						<div className="slide-button-text form-group col-xs-4">
							<input type="text" className="form-control" placeholder="buttonText" defaultValue={buttonText} ref="buttonText" />
						</div>
						<div className="slide-button-link form-group col-xs-4">
							<input type="text" className="form-control" placeholder="buttonLink" defaultValue={buttonLink} ref="buttonLink" />
						</div>
						<div className="col-sm-2"> 
						 <button type="submit" className="btn btn-default" onClick={this.openMedia}>Change background</button>  
						 {openOverlay}
						</div>
						<div className="form-group col-xs-2">
							<div className="btn-group"> 
								{buttons}
							</div>
						</div>
					</div>
								
				)
			}
});


var NewGalleryItem = React.createClass({
			
			mixins: [FluxMixin],

			getInitialState: function() {
				return {
					inActive: true
				};
			},

			activateMode: function (event) {
				event.preventDefault();
				this.setState({
					inActive: this.state.inActive? false : true
				});
				
			},

			closeAddItem: function () {
				this.setState({
					inActive: this.state.inActive? false : true
				});
				
			},

			render: function(){
					var mode = this.state.inActive?
								<div className="form-group pull-left">
									<button type="submit" className="btn btn-primary" onClick={this.activateMode}>Add</button>
								</div>:
								<GalleryEditItem addMode={true} closeAddItem={this.closeAddItem} />;

				return (
						<div id="add-category">
							{mode}
						</div>
							
				)
			}
});


var WriteGalleryContent = React.createClass({

	mixins: [FluxMixin],

	activateReadMode: function () {
		this.props.cancelHandler();
	},

	componentDidMount: function () {
			$(".navbar-footer").addClass("staticFooter");
	},

	render: function () {
		var list = [];
				
		if(this.props.items) {
			_.each(this.props.items, function (obj, index) {
					list.push(<GalleryEditItem obj={obj} />);
			});
		}


		return (
			<div className="clearfix">
			<div className="slide-images-list form-group clearfix">
					{list}
					<NewGalleryItem />
					<button type="submit" className="btn btn-default" onClick={this.props.cancelHandler}>Back</button>
			 </div>
			</div>);
	}
});


var ReadGalleryContent = React.createClass({

	activateSlider: function () {
			if(this.props.items) {
					$('.carousel').height($(".navbar-footer").position().top - $(".carousel").position().top);
			}
		
	},

	componentDidMount: function () {
			$(".navbar-footer").removeClass("staticFooter");
			this.activateSlider();
	},


	componentDidUpdate: function () {
		$(".navbar-footer").removeClass("staticFooter");
		this.activateSlider();
			
	},

	render: function () {
		var indicators = [], list = [];
		var admin = this.props.isAdmin?
			<div className="form-group clearfix editController">
					<button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
			 </div>:
			{};

		if(this.props.items) {
			_.each(this.props.items, function (obj, index) {
					var active = index === 0 ? "active" : "";
					var style = {
						backgroundImage: 'url(/uploads/' + (obj.image && obj.image.category.name) + "/" + obj.image.name + ')',
						width: "100%", 
						height: "100%",
						margin: "0",
						backgroundSize: "cover",
						backgroundPosition: "center top",
						backgroundRepeat: "no-repeat"
					};
							
					indicators.push(<li data-target="#carousel-example-generic" data-slide-to={index} className={active}></li>);
					list.push(
						<CarouselItem>
					
							{obj.image ?
								<div style={style}></div>
								:
									null}

							<div className="carousel-caption">
								<h1>{obj.headline}</h1>
								<h2>{obj.subheadline}</h2>
								<a className="shop-button btn btn-default" href={obj.buttonLink}>{obj.buttonText}</a>
							</div>
					
						</CarouselItem>
					);
			});
		}else {
			return (<div className="clearfix"></div>);
		}


		return (
			<div className="clearfix">
				<Carousel controls={true} indicators={true} interval={4000} pauseOnHover={true} slide={true} wrap={true}>
					{list}
				</Carousel>
				
				{admin}
			</div>
			
		);
	}
});


var GallerySection = React.createClass({

	mixins: [FluxMixin],

	getInitialState: function() {
		return {
			isReadMode: true
		};
	},

	activateReadMode: function () {
		this.setState({
			isReadMode: true
		});
	},
	
	editElement: function () {
		this.setState({
			isReadMode: false
		});
	},

	render: function () {
		
		var content = this.props.content;
		var mode = (this.props.isAdmin && !this.state.isReadMode)?

			<WriteGalleryContent         
				cancelHandler={this.activateReadMode}
				items={content} />:

			<ReadGalleryContent 
				isAdmin={this.props.isAdmin} 
				editHandler={this.editElement} 
				items={content} />;

		return (
			<div>
			 {mode}
			</div>
			
		);
	}
});

var IndexPage = React.createClass({
	mixins: [Router.State, FluxMixin, StoreWatchMixin("BaseContentStore", "GalleryStore")],

	getDefaultProps: function () {
		return {
			title: "Home",
		};
	},
	
	getStateFromFlux: function() {
		var baseContentStore = this.getFlux().store("BaseContentStore");
		var galleryStore = this.getFlux().store("GalleryStore");
		return {
			// TBD adapt edit mode to list
			 gallery: (galleryStore.gallery ? galleryStore.gallery : null)
		};
	},

	componentDidMount: function () {
		 
	},

 
	render: function () {
		var gallery = this.state.gallery?
				<GallerySection content={this.state.gallery} isAdmin={this.props.isAdmin} />:
			{};
		return (
			<div className="baseLayout clearfix">
				<div className="row">
				{gallery}
				</div>
			</div>
		);
	}
});

module.exports = IndexPage;