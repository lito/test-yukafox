/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
window.jQuery = require('jquery');
var React = require('react'),
	Editor = require('ainojs-reacti-editor'),
	EditorStyle = require('ainojs-reacti-editor-css'),
    Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var EditorWrapper = require('../components/EditorWrapper.jsx');

var EditBaseContent = React.createClass({

	getInitialState: function() {
		return {
			body: this.props.body
		};
	},

	updateElement: function () {
		this.props.saveHandler({
			title: (this.refs.title.getDOMNode().value || this.props.title),
			body: this.state.body,
			documentId: 0,
			link: "#",
			identifiedWith: "datenschutz"
		});
	},
	
	closeEdit: function () {
		this.props.cancelHandler();

	},

	onBodyChange: function (value) {
		this.setState({
			body: value
		});
	},

	render: function () {

	return (
		<div className="datenschutz-edit-section list-group-item row">
			<div className="form-group">
				<input type="text" className="form-control" placeholder="Title" defaultValue={this.props.title} ref="title" />
			</div>
			<div className="form-group">
				<label>Body</label>
				<EditorWrapper content={this.state.body} onChange={this.onBodyChange} />
			</div>
			<div className="form-group clearfix">
				<div className="btn-group">
					<button type="submit" className="btn btn-primary" onClick={this.updateElement}>Save</button>
					<button type="submit" className="btn btn-default" onClick={this.closeEdit}>Cancel</button>
				</div>
			</div>
		</div>
	);
	}
});

var ReadBaseContent = React.createClass({
	render: function () {
	var admin = this.props.isAdmin?
		<div className="form-group clearfix">
			<button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
		</div>:
		{};

	return (
		<div className="clearfix">
			<div className="clearfix">
				<h2>{this.props.title}</h2>
			</div>
			<div className="col-sm-12">
				<div className="floatToImg">
					<div dangerouslySetInnerHTML={{__html: this.props.body}}></div>
				</div>
				{admin}
			</div>
		</div>
	);
	}
});

var DatenschutzSection = React.createClass({

	mixins: [FluxMixin],

	getInitialState: function() {
		return {
			isReadMode: true
		};
	},

	saveElement: function (data) {
		this.getFlux().actions.content.editBaseContent(this.props.content.id, data, 'datenschutz');

		this.activateReadMode();
	},

	activateReadMode: function () {
		this.setState({
			isReadMode: true
		});
	},

	editElement: function () {
		this.setState({
			isReadMode: false
		});
	},

	render: function () {

		var content = this.props.content;
		var mode = (this.props.isAdmin && !this.state.isReadMode)?

		<EditBaseContent 
			saveHandler={this.saveElement} 
			cancelHandler={this.activateReadMode}
			title={content.title} 
			body={content.body} 
			items={content} />:

		<ReadBaseContent 
			isAdmin={this.props.isAdmin} 
			editHandler={this.editElement} 
			title={content.title} 
			body={content.body} 
			items={content} />;

		return (
			<div>
				{mode}
			</div>
		);
		}
});

var DatenschutzPage = React.createClass({
	mixins: [Router.State, FluxMixin, StoreWatchMixin("BaseContentStore")],

	getDefaultProps: function () {
		return {
			title: "Datenschutz",
		};
	},
	
	getStateFromFlux: function() {
		var baseContentStore = this.getFlux().store("BaseContentStore");
		return{
			datenschutz: (baseContentStore.contentList['datenschutz'] ? baseContentStore.contentList['datenschutz'][0] : null),
		}
	},

	componentDidMount: function () {

		if(!this.state.contentList) {
			this.getFlux().actions.content.loadBaseContent('datenschutz');
		}
	},

 
	render: function () {
		var datenschutz = this.state.datenschutz?
				<DatenschutzSection content={this.state.datenschutz} isAdmin={this.props.isAdmin} />:
			{};
		return (
			<div className="baseLayout clearfix">
				<div className="datenschutz-content centered">
					<div className="row">
						<div className="col-sm-8">
							{datenschutz}
						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = DatenschutzPage;