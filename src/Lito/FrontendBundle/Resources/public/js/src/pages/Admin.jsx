/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var React = require('react');
var Router = require('react-router');
var { Route, Redirect, RouteHandler, Link } = Router;
var AuthenticatedRouteMixin = require('./../mixins/AuthenticatedRouteMixin');

var AdminPage = React.createClass({
  mixins: [AuthenticatedRouteMixin],

  getDefaultProps: function () {
    return {
      sidebarTitle: "",
    };
  },

  render: function () {
   
    return (
      <div className="adminPage clearfix">
        <div className="row">
          <div className="AdminLeftPanel col-sm-3">
            <h3>{this.props.sidebarTitle}</h3>
            <div>
              <ul className="list-group">
                <li className="list-group-item"><Link to="settings">Settings</Link></li>
                <li className="list-group-item"><Link to="media">Media</Link></li>
                <li className="list-group-item"><Link to="messages">Messages</Link></li>
              </ul>
             </div>             
          </div>
          <div className="AdminRightPanel col-sm-8">
            <RouteHandler />
          </div>
        </div>  
        
        
      </div>
    );
  }
});

module.exports = AdminPage;